- There are few possible issues with a slow web application:

1. Server running all the components have resources starvation:
- i/o
- cpu
- memory

Since we assume the drives are SSD based - we can safely exclude i/o here

The other two factors shouldn't be excluded - it's quite possible that CPU utilization is quite high and different tasks waiting for the cpu resources

Since we run all the things on the same server - we may assume as well, that memory is not enough and system may start swapping - unloading process memory on disk and utilizing ssd drives as a temporary memory storage

These possibilities will be highly dependend on hardware resources of a system running our applications - DB, web server

If we take a look into software side of things - there are few items to check here:

1. Underlying layer - DB - possible reasons here can be big tables, slow queries, deadlock conditions in DB

2. Connectivity layer - connection between web server and DB - depends on type of connectivity used - either we are using unix sockets, either we are using TCP sockets - each of them have their own advantages

2.1 For the case with UNIX sockets - we should take a look into open files limits (ulimit)

2.2. For the case with TCP sockets - there is a hardwired limitation where we have only limited amount of IP, PORTs available to us

In a typical linux system without tuning range of local ports available to application - we usually have around ~32000 ports available to make outgoing connections

There could be few other things, like misconfigured security policies (selinux), local firewall misconfiguration

One tricky situation can be assymetric routing as well - usually happens on a system with a few interfaces, where answer to the client will be sent using a different path back

As for the investigation - I'll start from the top - checking ability of user to connect to the our application using common utilities like ping, curl, traceroute and go down the stack - check connectivity between user and us, and move down upon the stack
